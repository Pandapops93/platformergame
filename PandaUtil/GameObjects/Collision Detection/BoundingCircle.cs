﻿using PandaUtil.GameObjects;
using PandaUtil.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PandaUtil.CollisionDetection
{
    public class BoundingCircle : ICollisionDetector
    {
        public GameObject gameObject { get; set; }
        public void BindToObject(GameObject obj)
        {
            gameObject = obj;
        }

        public bool IsIntersecting(GameObject target)
        {
            var x1 = gameObject.XPos;
            var x2 = target.XPos;
            var y1 = gameObject.YPos;
            var y2 = target.YPos;

            var distance = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));

            if (distance > gameObject.Radius || distance > target.Radius)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
