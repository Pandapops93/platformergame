﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaUtil.GameObjects
{
    public static class TileMap
    {
        // the smaller these values the more accurate the pathing will be
        private const float TILE_HEIGHT = 1.0f;
        private const float TILE_WIDTH = 1.0f;
        private static bool Initiliased = false;
        private static float MapHeight { get; set; }
        private static float MapWidth { get; set; }

        private static int[,] TileArray { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mHeight">The height of the map in pixels</param>
        /// <param name="mWidth">The width of the map in pixels</param>
        public static void InitTileMap(float mHeight, float mWidth)
        {

            MapHeight = mHeight;
            MapWidth = mWidth;
            int height = (int)(mHeight / TILE_HEIGHT);
            int width = (int)(mWidth / TILE_WIDTH);

            TileArray = new int[height, width];
            Initiliased = true;

        }
        public static void MapObjectsToTileMap(List<GameObject> gameObjects)
        {

        }
        public static void AddGameObjectToMap(GameObject obj)
        {
            if (!Initiliased)
                throw new Exception("Tile Map not initialised");
            // find the game objects position on the tile map 
            // for now we will assume each game object takes up one tile.
            var xGridLoc = (int)(obj.XPos / TILE_WIDTH);
            var yGridLoc = (int)(obj.YPos / TILE_HEIGHT);

            obj.TileLocX = xGridLoc;
            obj.TileLocY = yGridLoc;
            TileArray[yGridLoc, xGridLoc] = 1;

        }

    }
}
