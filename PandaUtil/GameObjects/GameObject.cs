﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PandaUtil.GameObjects.Pathfinding;
using PandaUtil.Interfaces;



namespace PandaUtil.GameObjects
{
    public class GameObject
    {
        public ICollisionDetector ColDetector { get; set; }
        public IPathfinder PathFinder { get; set; }
        public float XPos { get; set; }
        public float YPos { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float Radius { get; set; }
        public int TileLocX { get; set; }
        public int TileLocY { get; set; }
        public Vector2 Pos
        {
            get
            {
                return new Vector2(XPos, YPos);
            }
        }
        public Texture2D Texture { get; set; }


        public GameObject(ICollisionDetector col, float x, float y, Texture2D tex) : this()
        {

            this.XPos = x;
            this.YPos = y;
            this.ColDetector = col;
            this.ColDetector.BindToObject(this);
            this.PathFinder = new AStar();
            this.PathFinder.BindToObject(this);
            TileMap.AddGameObjectToMap(this);
            this.Texture = tex;
        }



        public GameObject()
        {


        }

    }
}
