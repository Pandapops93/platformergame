﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PandaUtil.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaUtil.Helpers
{
    public static class Renderer
    {
        private static SpriteBatch _SpriteBatch { get; set; }


        public static void Init(GraphicsDevice device)
        {
            _SpriteBatch = new SpriteBatch(device);
        }

        public static void BeginDraw()
        {
            _SpriteBatch.Begin();
        }

        public static void Draw(GameObject obj)
        {
            _SpriteBatch.Draw(obj.Texture, obj.Pos, Color.White);
        }

        public static void EndDraw()
        {
            _SpriteBatch.End();
        }
    }
}
