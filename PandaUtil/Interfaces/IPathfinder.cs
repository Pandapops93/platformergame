﻿using PandaUtil.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaUtil.Interfaces
{
    public interface IPathfinder
    {
        GameObject source { get; set; }
        void BindToObject(GameObject obj);
        void PathToTarget(GameObject obj);
    }
}
