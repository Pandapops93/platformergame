﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PandaUtil.GameObjects;
namespace PandaUtil.Interfaces

{
    public interface ICollisionDetector
    {

        bool IsIntersecting(GameObject target);
        void BindToObject(GameObject obj);
    }
}
